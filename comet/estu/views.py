from django.shortcuts import render
from .models import Estu


def home(request):
  context = {
    'estus': Estu.objects.all().order_by('-date')
  }
  return render(request, 'estu/home.html', context)
