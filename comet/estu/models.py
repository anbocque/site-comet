from django.db import models

class Estu(models.Model):
  name = models.CharField(max_length=50)
  description = models.TextField()
  date = models.DateField()
  image = models.ImageField(default='default.png', upload_to='estu_pics')

  def __str__(self):
    return self.name
